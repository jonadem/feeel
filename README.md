Feeel
=====

Feeel is an open-source app for doing simple at-home exercises.

This is an in-progress rewrite of the [original app](https://gitlab.com/enjoyingfoss/feeel) in Flutter. It doesn't work as it should yet, but hopefully it will soon. *Get involved* to speed up the process!

To get a general idea of the app, see this video:
![YouTube video introduction](https://youtu.be/h1-HracNEWE)

Contribute
====
ANYONE can contribute, no special skills required. Message me on [Matrix](https://matrix.to/#/!jFShhgWHRXehKXrToU:matrix.org?via=matrix.org) if you'd like to help.

You can help by donating photos of exercises, turning photos into low-poly illustrations (easy stuff, anyone can do it), conducting user interviews, doing user testing, ideating, creating graphics, [translating](https://www.transifex.com/feeel/feeel/), spreading the message, programming, or doing anything else that would help the project. :)

Donate
====
If you'd prefer to donate instead, you can donate through these:
- **[Liberapay](https://liberapay.com/Feeel/)**
- [Bountysource](https://salt.bountysource.com/teams/feeel)